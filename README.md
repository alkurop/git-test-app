# Test Assignement

One Paragraph of project description goes here

### Task 1

Task 1 appears in the **task1** module

Pictures are saved into **PicturesRepo**.
Having an array of pictures, one should iteratively save same into the repo.
**PicturesRepo** lets you search the pictures taken on a specific date.
**PicturesRepoImpl** uses a Search Binary Tree to look for pictures.

With the first search request, repo creates the tree. 
Every next search request uses existing tree and works as quick as **O(log(n))**.

The test with 1000 items is done in the **test** directory.

### Task 2

Task 2 appears in the **app** module.

Application is written in MVVM manner.

I've chosen Kotlin as a language for this app.
