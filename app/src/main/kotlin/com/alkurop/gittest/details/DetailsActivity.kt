package com.alkurop.gittest.details

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.method.LinkMovementMethod
import android.text.util.Linkify
import android.view.MenuItem
import com.alkurop.gittest.R
import com.alkurop.gittest.entity.RepositoryViewPojo
import com.alkurop.gittest.entity.mapToViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import kotlinx.android.synthetic.main.activity_details.*
import java.text.DateFormat

class DetailsActivity : AppCompatActivity() {

    companion object {
        const val KEY_ID = "key_id"
    }

    val dis = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val id = intent.getLongExtra(KEY_ID, 0)
        val model = ViewModelProviders.of(this).get(DetailsViewModel::class.java)
        dis += model.loadData(id).subscribe { item -> showViewData(item.mapToViewModel()) }
    }

    private fun showViewData(item: RepositoryViewPojo) {
        with(item) {
            supportActionBar?.title = title
            descriptionView.text = description
            languageView.text = language
            starView.text = "$starCount"
            issuesView.text = "$issueCount"
            whatchersView.text = "$watcherCount"
            updateView.text = "${updated?.let { DateFormat.getInstance().format(it) }}"
            forkView.text = "$forkCount"
            urlView.text = url
            Linkify.addLinks(urlView, Linkify.WEB_URLS)
            urlView.movementMethod = LinkMovementMethod.getInstance()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }
}
