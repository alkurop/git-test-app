package com.alkurop.gittest.details

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import com.alkurop.gittest.App
import com.alkurop.gittest.cache.RepositoryCacheDao
import com.alkurop.gittest.cache.RepositoryCachePojo
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class DetailsViewModel(application: Application) : AndroidViewModel(application) {

    private val repositoryCacheDao: RepositoryCacheDao
    private val disposable = CompositeDisposable()
    private var dataSingle: Single<RepositoryCachePojo>? = null

    init {
        val dependencyProvider = (application as App).getDependencyProvider()
        this.repositoryCacheDao = dependencyProvider.provideRepositoryDao()
    }

    fun loadData(id: Long): Single<RepositoryCachePojo> {
        if (dataSingle == null) {
            dataSingle = Single.fromCallable({ repositoryCacheDao.getById(id) })
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread()).cache()
        }
        return dataSingle!!
    }

    override fun onCleared() {
        disposable.clear()
    }
}
