package com.alkurop.gittest

import android.app.Application

open class App : Application() {

    private lateinit var dependencyProvider: DependencyProvider

    override fun onCreate() {
        super.onCreate()
        dependencyProvider = DependencyProviderImpl(this)
    }

    open fun getDependencyProvider() = dependencyProvider
}
