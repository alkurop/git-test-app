package com.alkurop.gittest

import com.alkurop.gittest.api.RepoApi
import com.alkurop.gittest.cache.RepositoryCacheDao

interface DependencyProvider {

    fun provideRepoApi(): RepoApi

    fun provideRepositoryDao(): RepositoryCacheDao
}
