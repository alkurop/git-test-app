package com.alkurop.gittest.cache

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase

@Database(entities = arrayOf(RepositoryCachePojo::class), version = 1)
abstract class CacheDatabase : RoomDatabase() {

    abstract fun repositoryCacheDao(): RepositoryCacheDao
}