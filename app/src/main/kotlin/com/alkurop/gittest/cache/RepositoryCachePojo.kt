package com.alkurop.gittest.cache

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.alkurop.gittest.cache.RepositoryCacheContract.COLUMN_FORK_COUNT
import com.alkurop.gittest.cache.RepositoryCacheContract.COLUMN_ISSUE_COUNT
import com.alkurop.gittest.cache.RepositoryCacheContract.COLUMN_LANGUAGE
import com.alkurop.gittest.cache.RepositoryCacheContract.COLUMN_PAGE
import com.alkurop.gittest.cache.RepositoryCacheContract.COLUMN_RESCRIPTION
import com.alkurop.gittest.cache.RepositoryCacheContract.COLUMN_STAR_COUNT
import com.alkurop.gittest.cache.RepositoryCacheContract.COLUMN_TITLE
import com.alkurop.gittest.cache.RepositoryCacheContract.COLUMN_UPDATED_AT
import com.alkurop.gittest.cache.RepositoryCacheContract.COLUMN_URL
import com.alkurop.gittest.cache.RepositoryCacheContract.COLUMN_WHATCHER_COUNT
import com.alkurop.gittest.cache.RepositoryCacheContract.REPOSITORY_TABLE

@Entity(tableName = REPOSITORY_TABLE)
class RepositoryCachePojo {
    @PrimaryKey
    var id: Long = -1

    @ColumnInfo(name = COLUMN_TITLE)
    var title: String? = null

    @ColumnInfo(name = COLUMN_RESCRIPTION)
    var description: String? = null

    @ColumnInfo(name = COLUMN_STAR_COUNT)
    var starCount: Int? = null

    @ColumnInfo(name = COLUMN_FORK_COUNT)
    var forkCount: Int? = null

    @ColumnInfo(name = COLUMN_WHATCHER_COUNT)
    var whatcherCount: Int? = null

    @ColumnInfo(name = COLUMN_ISSUE_COUNT)
    var issueCount: Int? = null

    @ColumnInfo(name = COLUMN_LANGUAGE)
    var language: String? = null

    @ColumnInfo(name = COLUMN_UPDATED_AT)
    var updatedAt: String? = null

    @ColumnInfo(name = COLUMN_URL)
    var url: String? = null

    @ColumnInfo(name = COLUMN_PAGE)
    var page: Int = -1
}

object RepositoryCacheContract {
    const val REPOSITORY_TABLE = "repository_table"
    const val COLUMN_LANGUAGE = "column_language"
    const val COLUMN_TITLE = "column_title"
    const val COLUMN_RESCRIPTION = "column_description"
    const val COLUMN_STAR_COUNT = "column_star_count"
    const val COLUMN_FORK_COUNT = "column_fork_count"
    const val COLUMN_WHATCHER_COUNT = "column_whatcher_count"
    const val COLUMN_ISSUE_COUNT = "column_issue_count"
    const val COLUMN_UPDATED_AT = "column_updated_at"
    const val COLUMN_URL = "column_url"
    const val COLUMN_PAGE = "column_page"
}
