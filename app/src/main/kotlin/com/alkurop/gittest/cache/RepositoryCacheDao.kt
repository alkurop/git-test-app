package com.alkurop.gittest.cache

import android.arch.paging.LivePagedListProvider
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query

@Dao
interface RepositoryCacheDao {

    @Query("SELECT * FROM ${RepositoryCacheContract.REPOSITORY_TABLE} ORDER BY ${RepositoryCacheContract.COLUMN_TITLE}")
    fun getAll(): LivePagedListProvider<Int, RepositoryCachePojo>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(repositories: Array<RepositoryCachePojo>)

    @Query("SELECT * FROM ${RepositoryCacheContract.REPOSITORY_TABLE} WHERE id = :id ")
    fun getById(id: Long): RepositoryCachePojo

    @Query("DELETE FROM ${RepositoryCacheContract.REPOSITORY_TABLE} WHERE  ${RepositoryCacheContract.COLUMN_PAGE} = :page")
    fun deletePage(page: Int)
}