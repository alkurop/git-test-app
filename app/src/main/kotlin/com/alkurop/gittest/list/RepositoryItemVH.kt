package com.alkurop.gittest.list

import android.support.v7.widget.RecyclerView
import android.view.View
import com.alkurop.gittest.entity.RepositoryViewPojo
import kotlinx.android.synthetic.main.item_repository.view.*
import java.text.DateFormat

class RepositoryItemVH(itemView: View,
                       val onItemClickListener: ((Long) -> Unit)?) : RecyclerView.ViewHolder(itemView) {

    fun bind(model: RepositoryViewPojo) {
        with(model) {
            itemView.setOnClickListener { onItemClickListener?.invoke(model.id) }
            itemView.titleView.text = title
            itemView.descriptionView.text = description
            itemView.languageView.text = language
            itemView.starView.text = "${starCount}"
            itemView.forkView.text = "${forkCount}"
            itemView.updateView.text = "${updated?.let { DateFormat.getInstance().format(it) }}"
        }
    }
}