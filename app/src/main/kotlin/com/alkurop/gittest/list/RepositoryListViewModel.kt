package com.alkurop.gittest.list

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.paging.PagedList
import com.alkurop.gittest.App
import com.alkurop.gittest.api.RepoApi
import com.alkurop.gittest.cache.RepositoryCacheDao
import com.alkurop.gittest.cache.RepositoryCachePojo
import com.alkurop.gittest.entity.mapToCacheModel
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import timber.log.Timber

class RepositoryListViewModel(application: Application) : AndroidViewModel(application) {

    val usersList: LiveData<PagedList<RepositoryCachePojo>>
    private val api: RepoApi
    private val repositoryCacheDao: RepositoryCacheDao
    private val dis = CompositeDisposable()
    private val errorPublisher = PublishSubject.create<Throwable>()
    private val loadingPublisher = BehaviorSubject.create<Boolean>()

    private var currentPage = 0
    private var canLoadMore = true

    init {
        val dependencyProvider = (application as App).getDependencyProvider()
        this.repositoryCacheDao = dependencyProvider.provideRepositoryDao()
        val all = repositoryCacheDao.getAll()
        usersList = all.create(0, 50)
        api = dependencyProvider.provideRepoApi()
    }

    fun loadFirstTimeData() {
        if (canLoadMore && currentPage == 0) {
            load()
        }
    }

    fun load() {
        if (canLoadMore) {
            canLoadMore = false
            dis += api.getRepos(currentPage)
                    .doOnSubscribe { loadingPublisher.onNext(true) }
                    .doAfterTerminate { loadingPublisher.onNext(false) }
                    .subscribeOn(Schedulers.io())
                    .subscribe({
                        currentPage++
                        canLoadMore = it.isNotEmpty()
                        repositoryCacheDao.deletePage(currentPage)
                        repositoryCacheDao.insertAll(it.map {
                            it.mapToCacheModel(currentPage)
                        }.toTypedArray())
                    }, {
                        Timber.e(it)
                        errorPublisher.onNext(it)
                        canLoadMore = false
                    })
        }
    }

    fun retry() {
        canLoadMore = true
        load()
    }

    override fun onCleared() {
        dis.clear()
    }

    fun errors(): Observable<Throwable> = errorPublisher

    fun loadingState(): Observable<Boolean> = loadingPublisher
}
