package com.alkurop.gittest.list

import android.support.v7.recyclerview.extensions.DiffCallback
import com.alkurop.gittest.cache.RepositoryCachePojo

class RepositoryListDiff : DiffCallback<RepositoryCachePojo>() {

    override fun areContentsTheSame(oldItem: RepositoryCachePojo, newItem: RepositoryCachePojo): Boolean {
        return newItem.updatedAt == oldItem.updatedAt
    }

    override fun areItemsTheSame(oldItem: RepositoryCachePojo, newItem: RepositoryCachePojo): Boolean {
        return newItem.id == oldItem.id
    }
}
