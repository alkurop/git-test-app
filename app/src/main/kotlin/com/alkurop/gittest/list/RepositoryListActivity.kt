package com.alkurop.gittest.list

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.arch.paging.PagedList
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.alkurop.gittest.R
import com.alkurop.gittest.cache.RepositoryCachePojo
import com.alkurop.gittest.details.DetailsActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import kotlinx.android.synthetic.main.activity_repository_list.*

class RepositoryListActivity : AppCompatActivity() {

    val dis = CompositeDisposable()
    lateinit var viewModel: RepositoryListViewModel
    lateinit var adapter: RepositoryListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_repository_list)
        setSupportActionBar(toolbar)
        supportActionBar?.title = getString(R.string.app_name)
        viewModel = ViewModelProviders.of(this).get(RepositoryListViewModel::class.java)

        initList()
        setListeners()
        viewModel.loadFirstTimeData()
    }

    fun initList() {
        val linearLayoutManager = LinearLayoutManager(this)
        recyclerView.addItemDecoration(DividerDecorator(R.dimen.repo_list_divider_height))
        recyclerView.layoutManager = linearLayoutManager

        adapter = RepositoryListAdapter()
        recyclerView.adapter = adapter
        adapter.loadMoreListener = { viewModel.load() }
        adapter.onItemClickListener = {
            val intent = Intent(this, DetailsActivity::class.java)
            intent.putExtra(DetailsActivity.KEY_ID, it)
            startActivity(intent)
        }
        viewModel.usersList.observe(this, Observer<PagedList<RepositoryCachePojo>> {
            adapter.setList(it)

        })
    }

    fun setListeners() {
        dis += viewModel
                .errors()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    Snackbar.make(root, it.localizedMessage, Snackbar.LENGTH_SHORT).show()
                    showEmptyView(adapter.itemCount == 0)
                }

        dis += viewModel
                .loadingState().filter({ it })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    swipeView.isRefreshing.not().run { showLoadingView(true) }
                }

        dis += viewModel
                .loadingState().filter({ !it })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    showLoadingView(false)
                }

        emptyView.setOnClickListener {
            viewModel.retry()
            showEmptyView(false)
        }
        swipeView.setOnRefreshListener { viewModel.retry() }
    }

    override fun onDestroy() {
        super.onDestroy()
        dis.clear()
    }

    fun showLoadingView(isVisible: Boolean) {
        val listIsEmpty = adapter.itemCount == 0
        if (isVisible && !swipeView.isRefreshing && listIsEmpty) {
            loadingView.visibility = View.VISIBLE
        }
        if (!isVisible) {
            loadingView.visibility = View.GONE
            swipeView.isRefreshing = false
        }
    }

    fun showEmptyView(isVisible: Boolean) {
        emptyView.visibility = if (isVisible) View.VISIBLE else View.GONE
    }
}
