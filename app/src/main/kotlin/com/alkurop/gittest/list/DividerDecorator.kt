package com.alkurop.gittest.list

import android.graphics.Rect
import android.support.annotation.DimenRes
import android.support.v7.widget.RecyclerView
import android.view.View

internal class DividerDecorator(@DimenRes val mPaddingRes: Int) : RecyclerView.ItemDecoration() {

    private var mPadding: Int = 0

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView,
                                state: RecyclerView.State?) {
        if (mPadding == 0) {
            mPadding = view.context.resources.getDimensionPixelSize(mPaddingRes)
        }
        if (parent.getChildAdapterPosition(view) != parent.adapter.itemCount - 1) {
            outRect.bottom = mPadding
        }
    }
}
