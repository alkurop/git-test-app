package com.alkurop.gittest.list

import android.arch.paging.PagedListAdapter
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.alkurop.gittest.R
import com.alkurop.gittest.cache.RepositoryCachePojo
import com.alkurop.gittest.entity.mapToViewModel

class RepositoryListAdapter : PagedListAdapter<RepositoryCachePojo, RepositoryItemVH>(RepositoryListDiff()) {

    companion object {
        val ITEMS_REQUEST_MORE = 3
    }

    lateinit var li: LayoutInflater
    var onItemClickListener: ((Long) -> Unit)? = null
    var loadMoreListener: (() -> Unit)? = null

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        li = LayoutInflater.from(recyclerView.context)
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val lastItem = (recyclerView.layoutManager as LinearLayoutManager).findLastVisibleItemPosition()
                val itemCount = itemCount
                if (lastItem != 0 && itemCount - ITEMS_REQUEST_MORE < lastItem) {
                    loadMoreListener?.invoke()
                }
            }
        })

    }

    override fun onBindViewHolder(holder: RepositoryItemVH?, position: Int) {
        holder?.bind(getItem(position)!!.mapToViewModel())
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RepositoryItemVH {
        val view = li.inflate(R.layout.item_repository, parent, false)
        return RepositoryItemVH(view, onItemClickListener)
    }

}
