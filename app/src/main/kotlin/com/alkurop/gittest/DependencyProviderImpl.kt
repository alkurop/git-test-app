package com.alkurop.gittest

import android.app.Application
import android.arch.persistence.room.Room
import com.alkurop.gittest.api.RepoApi
import com.alkurop.gittest.api.RepositoryRetrofit
import com.alkurop.gittest.cache.CacheDatabase
import com.alkurop.gittest.cache.RepositoryCacheDao

class DependencyProviderImpl(application: Application) : DependencyProvider {

    private val repoApi = RepositoryRetrofit().api
    private val repoDao = Room
            .databaseBuilder(application, CacheDatabase::class.java, CacheDatabase::javaClass.name)
            .build().repositoryCacheDao()

    override fun provideRepoApi(): RepoApi = repoApi

    override fun provideRepositoryDao(): RepositoryCacheDao = repoDao

}
