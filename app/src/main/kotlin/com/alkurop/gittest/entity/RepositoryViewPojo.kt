package com.alkurop.gittest.entity

import java.sql.Timestamp

data class RepositoryViewPojo(
        val id: Long,
        val title: String,
        val description: String,
        val starCount: Int,
        val forkCount: Int,
        val watcherCount: Int,
        val issueCount: Int,
        val language: String,
        val url: String,
        val updated: Timestamp?)
