package com.alkurop.gittest.entity

import com.alkurop.gittest.api.RepositoryApiPojo
import com.alkurop.gittest.cache.RepositoryCachePojo
import java.sql.Timestamp

fun RepositoryCachePojo.mapToViewModel(): RepositoryViewPojo {
    return RepositoryViewPojo(this.id,
            this.title ?: "",
            this.description ?: "",
            this.starCount ?: 0,
            this.forkCount ?: 0,
            this.whatcherCount ?: 0,
            this.issueCount ?: 0,
            this.language ?: "",
            this.url ?: "",
            if (this.updatedAt != null) Timestamp.valueOf(this.updatedAt) else null)
}

fun RepositoryApiPojo.mapToCacheModel(page:Int): RepositoryCachePojo {
    val repositoryCacheModel = RepositoryCachePojo()
    repositoryCacheModel.id = id
    repositoryCacheModel.title = name
    repositoryCacheModel.description = description
    repositoryCacheModel.whatcherCount = watchersCount
    repositoryCacheModel.forkCount = forksCount
    repositoryCacheModel.issueCount = openIssuesCount
    repositoryCacheModel.starCount = starCount
    repositoryCacheModel.updatedAt = updatedAt?.toString()
    repositoryCacheModel.language = language
    repositoryCacheModel.url = htmlUrl
    repositoryCacheModel.page = page
    return repositoryCacheModel
}
