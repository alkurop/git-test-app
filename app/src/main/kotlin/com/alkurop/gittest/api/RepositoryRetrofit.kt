package com.alkurop.gittest.api

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RepositoryRetrofit {
    companion object {
        const val API_URL = "https://api.github.com"
        val SECRET = Pair("client_secret", "5e21809b0ccabb42bc6550f8003e38465c43f7d8")
        val ID = Pair("client_id", "90fbeb2dcceb671971d7")
    }

    val api: RepoApi

    init {
        val client = OkHttpClient.Builder()
                .addInterceptor { chain ->
                    var request = chain.request()
                    val url = request.url().newBuilder().addQueryParameter(SECRET.first, SECRET.second)
                            .addQueryParameter(ID.first, ID.second).build()
                    request = request.newBuilder().url(url).build()
                    chain.proceed(request)

                }
                .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BASIC))
                .build()

        val retrofit = Retrofit.Builder().baseUrl(API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
                .build()
        api = retrofit.create(RepoApi::class.java)
    }
}
