package com.alkurop.gittest.api

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface RepoApi {

    @GET("users/jakewharton/repos")
    fun getRepos(@Query("page") page: Int): Single<List<RepositoryApiPojo>>
}