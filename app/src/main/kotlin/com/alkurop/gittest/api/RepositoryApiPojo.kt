package com.alkurop.gittest.api

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.sql.Timestamp

data class RepositoryApiPojo(
        @SerializedName("id")
        @Expose
        val id: Long,

        @SerializedName("name")
        @Expose
        val name: String,

        @SerializedName("full_name")
        @Expose
        val fullName: String?,

        @SerializedName("html_url")
        @Expose
        val htmlUrl: String?,

        @SerializedName("description")
        @Expose
        val description: String?,

        @SerializedName("watchers_count")
        @Expose
        val watchersCount: Int?,

        @SerializedName("forks_count")
        @Expose
        val forksCount: Int?,

        @SerializedName("stargazers_count")
        @Expose
        val starCount: Int?,

        @SerializedName("open_issues_count")
        @Expose
        val openIssuesCount: Int?,

        @SerializedName("language")
        @Expose
        val language: String?,

        @SerializedName("updated_at")
        @Expose
        val updatedAt: Timestamp?)
