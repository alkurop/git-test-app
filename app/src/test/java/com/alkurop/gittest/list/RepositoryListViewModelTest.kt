package com.alkurop.gittest.list

import android.arch.lifecycle.MutableLiveData
import android.arch.paging.LivePagedListProvider
import android.arch.paging.PagedList
import com.alkurop.gittest.App
import com.alkurop.gittest.DependencyProvider
import com.alkurop.gittest.api.RepoApi
import com.alkurop.gittest.cache.RepositoryCacheDao
import com.alkurop.gittest.cache.RepositoryCachePojo
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.times
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.TestScheduler
import org.assertj.core.api.Java6Assertions.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import java.util.concurrent.TimeUnit

class RepositoryListViewModelTest {
    @Mock lateinit var dependencyProvider: DependencyProvider
    @Mock lateinit var api: RepoApi
    @Mock lateinit var dao: RepositoryCacheDao
    @Mock lateinit var liveListProvider: LivePagedListProvider<Int, RepositoryCachePojo>

    lateinit var model: RepositoryListViewModel
    lateinit var mutableLiveData: MutableLiveData<PagedList<RepositoryCachePojo>>
    val testScheduler = TestScheduler()

    @Before
    fun initMocks() {
        RxJavaPlugins.setIoSchedulerHandler { testScheduler }
        MockitoAnnotations.initMocks(this)

        mutableLiveData = MutableLiveData<PagedList<RepositoryCachePojo>>()
        whenever(liveListProvider.create(any(), any<Int>())).thenReturn(mutableLiveData)
        whenever(dao.getAll()).thenReturn(liveListProvider)
        whenever(dependencyProvider.provideRepoApi()).thenReturn(api)
        whenever(dependencyProvider.provideRepositoryDao()).thenReturn(dao)
        whenever(api.getRepos(any())).thenReturn(Single.never())

        model = RepositoryListViewModel(object : App() {
            override fun getDependencyProvider() = dependencyProvider

        })
    }

    @After
    fun tearDown() {
        RxJavaPlugins.reset()
    }

    @Test
    fun tesLoadFirstTimeOnce() {
        model.loadFirstTimeData()
        model.loadFirstTimeData()

        verify(api).getRepos(any())
    }

    @Test
    fun testRetry() {
        model.loadFirstTimeData()
        model.retry()

        verify(api, times(2)).getRepos(any())
    }

    @Test
    fun testLoadOk() {
        whenever(api.getRepos(any())).thenReturn(Single.just(listOf()))
        model.loadFirstTimeData()

        val testLoading = model.loadingState().test()
        testScheduler.advanceTimeBy(100, TimeUnit.DAYS)
        testLoading.assertNoErrors()
        testLoading.assertNotComplete()

        val values = testLoading.values()
        assertThat(values.size).isEqualTo(2)
        assertThat(values[0]).isEqualTo(true)
        assertThat(values[1]).isEqualTo(false)

        val testErrors = model.errors().test()
        testErrors.assertNoValues()
        testErrors.assertNoErrors()
        testErrors.assertNotComplete()

        verify(dao).insertAll(any())

    }

    @Test
    fun testLoadError() {
        val error = IllegalStateException("expected")
        whenever(api.getRepos(any())).thenReturn(Single.error(error))

        val testLoading = model.loadingState().test()
        val testErrors = model.errors().test()

        model.loadFirstTimeData()
        testScheduler.advanceTimeBy(100, TimeUnit.DAYS)

        val values = testLoading.values()

        testLoading.assertNoErrors()
        testLoading.assertNotComplete()

        assertThat(values.size).isEqualTo(2)
        assertThat(values[0]).isEqualTo(true)
        assertThat(values[1]).isEqualTo(false)

        testErrors.assertValue(error)
        testErrors.assertNoErrors()
        testErrors.assertNotComplete()

        verify(dao, times(0)).insertAll(any())
    }

}