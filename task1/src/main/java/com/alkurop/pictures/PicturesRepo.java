package com.alkurop.pictures;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public interface PicturesRepo {

    void addPicture(@NonNull Picture picture);

    @NonNull
    Picture[] findPictures(@NonNull String date);
}
