package com.alkurop.pictures;

public class Preconditions {

    public static <T> T notNull(T item) {
        if (item == null) {
            throw new IllegalStateException("item == null");
        } else {
            return item;
        }
    }

    public static long notZero(long item) {
        if (item == 0) {
            throw new IllegalStateException("item == 0");
        } else {
            return item;
        }
    }
}
