package com.alkurop.pictures;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

class IndexNode<K extends Comparable<K>> {

    @Nullable
    IndexNode<K> left, right;

    @NonNull
    final K key;

    @NonNull
    final Picture value;

    IndexNode(@NonNull K key, @NonNull Picture value) {
        this.key = Preconditions.notNull(key);
        this.value = Preconditions.notNull(value);
    }
}