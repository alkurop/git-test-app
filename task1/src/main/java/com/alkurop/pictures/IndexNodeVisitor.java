package com.alkurop.pictures;

import android.support.annotation.NonNull;

public interface IndexNodeVisitor<T extends Comparable<T>> {

    void visit(@NonNull IndexNode<T> item);

}
