package com.alkurop.pictures;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public abstract class IndexNodeTree<K extends Comparable<K>> {

    @Nullable
    private IndexNode<K> root;

    abstract void addItem(@NonNull Picture picture);

    void addItem(@NonNull K key,
                 @NonNull Picture value) {
        Preconditions.notNull(key);
        Preconditions.notNull(value);
        root = addItemRec(root, key, value);
    }

    private IndexNode<K> addItemRec(@Nullable IndexNode<K> root,
                                    @NonNull K key,
                                    @NonNull Picture value) {
        Preconditions.notNull(key);
        Preconditions.notNull(value);
        if (root == null) return new IndexNode<>(key, value);
        if (root.key.compareTo(key) >= 0) {
            root.left = addItemRec(root.left, key, value);
        } else {
            root.right = addItemRec(root.right, key, value);
        }
        return root;
    }

    void traverseInOrderInBounds(@NonNull IndexNodeVisitor<K> visitor,
                                 @Nullable K lowerInclusive,
                                 @Nullable K higherInclusive) {
        Preconditions.notNull(visitor);
        traverseInOrderParent(root, visitor, lowerInclusive, higherInclusive);
    }

    private void traverseInOrderParent(@Nullable IndexNode<K> node,
                                       @NonNull IndexNodeVisitor<K> visitor,
                                       @Nullable K lowerInclusive,
                                       @Nullable K higherInclusive) {
        Preconditions.notNull(visitor);
        if (node != null) {
            boolean lowerBoundCondition = lowerBoundCondition(node, lowerInclusive);
            boolean higherBoundCondition = higherBoundCondition(node, higherInclusive);

            if (lowerBoundCondition) {
                traverseInOrderParent(node.left, visitor, lowerInclusive, higherInclusive);
            }
            if (higherBoundCondition && lowerBoundCondition) {
                visitor.visit(node);
            }
            if (higherBoundCondition) {
                traverseInOrderParent(node.right, visitor, lowerInclusive, higherInclusive);
            }
        }
    }

    private boolean lowerBoundCondition(@NonNull IndexNode<K> node,
                                        @Nullable K lowerInclusive) {
        Preconditions.notNull(node);
        return lowerInclusive == null || node.key.compareTo(lowerInclusive) >= 0;
    }

    private boolean higherBoundCondition(@NonNull IndexNode<K> node,
                                         @Nullable K higherInclusive) {
        Preconditions.notNull(node);
        return higherInclusive == null || node.key.compareTo(higherInclusive) <= 0;
    }
}
