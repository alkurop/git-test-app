package com.alkurop.pictures;

import java.time.Instant;

public class Picture {
    private static final String JPEG_FILE_EXT = ".jpg";
    private long id, taken_at;
    private String fileName;

    public Picture(long taken_at) {
        this.taken_at = taken_at;
    }

    public void setId(long id) {
        this.id = id;
        this.fileName = String.format("%d%s", id, JPEG_FILE_EXT);
    }

    public long getId() {
        return id;
    }

    public long getTaken_at() {
        return taken_at;
    }

    public String getFileName() {
        return fileName;
    }

    @Override
    public String toString() {
        return "Picture{" +
                "id=" + id +
                ", taken_at=" + Instant.ofEpochMilli(taken_at) +
                ", fileName='" + fileName + '\'' +
                '}';
    }
}
