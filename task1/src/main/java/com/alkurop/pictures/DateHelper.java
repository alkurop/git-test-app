package com.alkurop.pictures;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.concurrent.ThreadLocalRandom;

public class DateHelper {
    private static final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ISO_LOCAL_DATE;

    @NonNull
    public Instant fromString(@NonNull String date) {
        TemporalAccessor parse = TIME_FORMATTER
                .parse(Preconditions.notNull(date));
        LocalDate from = LocalDate.from(parse);
        return from.atTime(0, 0, 0)
                .toInstant(ZoneOffset.UTC);
    }

    @Nullable
    public Instant getRandomDate(Instant from, Instant to) {
        Instant instant = null;
        while (instant == null) {
            int[] randomYearMonthDay = getRandomYearMonthDay(from, to);
            int randomYear = randomYearMonthDay[0];
            int randomMonth = randomYearMonthDay[1];
            int randomDay = randomYearMonthDay[2];
            try {
                instant = LocalDate.of(randomYear, randomMonth, randomDay)
                        .atTime(0, 0, 0)
                        .toInstant(ZoneOffset.UTC);
            } catch (Exception e) {
                instant = null;
            }
        }

        return instant;
    }

    private int[] getRandomYearMonthDay(Instant from, Instant to) {
        int[] year = new int[] {
                LocalDateTime.ofInstant(from, ZoneOffset.UTC).getYear(),
                LocalDateTime.ofInstant(to, ZoneOffset.UTC).getYear()
        };

        int[] month = new int[] {
                LocalDateTime.ofInstant(from, ZoneOffset.UTC).getMonth().getValue(),
                LocalDateTime.ofInstant(to, ZoneOffset.UTC).getMonth().getValue()
        };

        int[] day = new int[] {
                LocalDateTime.ofInstant(from, ZoneOffset.UTC).getDayOfMonth(),
                LocalDateTime.ofInstant(to, ZoneOffset.UTC).getDayOfMonth()
        };

        int randomYear = ThreadLocalRandom.current().nextInt(year[0], year[1] + 1);
        int randomMonth = ThreadLocalRandom.current().nextInt(month[0], month[1] + 1);
        int randomDay = ThreadLocalRandom.current().nextInt(day[0], day[1] + 1);

        return new int[] { randomYear, randomMonth, randomDay };
    }
}
