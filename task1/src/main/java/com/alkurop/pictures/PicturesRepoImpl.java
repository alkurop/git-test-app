package com.alkurop.pictures;

import android.support.annotation.NonNull;

import java.util.HashMap;
import java.util.LinkedList;

public class PicturesRepoImpl implements PicturesRepo {
    private static final String CACHE_NAME_TIMESTAMP = "timestamp";
    private final HashMap<Long, Picture> pictures = new HashMap<>();
    private final HashMap<String, IndexNodeTree> searchCacheMap = new HashMap<>();

    @Override

    public void addPicture(@NonNull Picture picture) {
        Preconditions.notNull(picture);
        Preconditions.notZero(picture.getTaken_at());
        long pictureId = pictures.size() + 1;
        picture.setId(pictureId);
        pictures.put(pictureId, picture);
        for (IndexNodeTree indexNodeTree : searchCacheMap.values()) {
            indexNodeTree.addItem(picture);
        }
    }

    @Override
    @NonNull
    public Picture[] findPictures(@NonNull String date) {
        Preconditions.notNull(date);
        long milli = new DateHelper().fromString(date).toEpochMilli();
        return findPicturesTimeStamp(milli, milli);
    }

    private Picture[] findPicturesTimeStamp(long lowInclusive, long highInclusive) {

        final IndexNodeTree<Long> tree;
        if (searchCacheMap.containsKey(CACHE_NAME_TIMESTAMP)) {
            tree = ((IndexNodeTree<Long>) searchCacheMap.get(CACHE_NAME_TIMESTAMP));
        } else {
            tree = new IndexNodeTree<Long>() {
                @Override
                void addItem(@NonNull Picture picture) {
                    Preconditions.notNull(picture);
                    addItem(picture.getTaken_at(), picture);
                }
            };

            searchCacheMap.put(CACHE_NAME_TIMESTAMP, tree);
            for (Picture picture : pictures.values()) {
                tree.addItem(picture);
            }
        }

        final LinkedList<Picture> pictures = new LinkedList<>();
        tree.traverseInOrderInBounds(item -> pictures.add(item.value), lowInclusive, highInclusive);

        Picture[] picturesArray = new Picture[pictures.size()];
        pictures.toArray(picturesArray);
        return picturesArray;
    }
}
