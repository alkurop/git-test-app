package com.alkurop.pictures;

import org.junit.Test;

import java.time.Instant;

public class PicturesTest {
    PicturesRepo repo;

    @Test
    public void picturesTest() {
        repo = new PicturesRepoImpl();
        int itemCount = 999;
        DateHelper dateHelper = new DateHelper();
        Instant from = dateHelper.fromString("2016-01-01");
        Instant to = dateHelper.fromString("2017-12-31");
        for (int i = 0; i < itemCount; i++) {
            Instant randomDate = dateHelper.getRandomDate(from, to);
            Picture picture = new Picture(randomDate.toEpochMilli());
            repo.addPicture(picture);
        }

        Picture[] pictures = repo.findPictures("2017-12-21");
        for (Picture picture : pictures) {
            System.out.println(picture.toString());
        }

    }
}
